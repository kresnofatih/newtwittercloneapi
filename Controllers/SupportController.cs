using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NewTwitterCloneApi.Constants;
using NewTwitterCloneApi.Data;
using NewTwitterCloneApi.Models;
using NewTwitterCloneApi.Models.DTOs;
using NewTwitterCloneApi.Utilities;

namespace NewTwitterCloneApi.Controllers
{
    [NonController]
    public class SupportController : ControllerBase
    {
        public AppDbContext _appDbContext;

        public SupportController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<Account> GetAccountById(string accountId)
        {
            var selectedAccount = await _appDbContext.Accounts.FindAsync(accountId);
            return selectedAccount;
        }

        public bool GetAccountExists(Account account)
        {
            if(account != null)
            {
                return true;
            }
            return false;
        }

        public bool GetRelationExists(Relation relation)
        {
            if(relation != null)
            {
                return true;
            }
            return false;
        }

        public bool GetBlacklistStatus(Account account)
        {
            if(account.Type != AccountTypes.Blacklisted)
            {
                return false;
            }
            return true;
        }
        
        public bool GetPasswordIsCorrect(LoginAccountReq loginAccount, Account account)
        {
            var loginPassword = loginAccount.Password;
            var hashedPassword = account.HashedPassword;
            return HashUtility.GetPasswordsMatch(loginPassword, hashedPassword);
        }

        public string GetRequestAccessToken()
        {
            return Request.Headers[HeaderTypes.AccessToken];
        }

        public async Task<CredentialsRes> GetCredentialsRes(string accountId)
        {
            var selectedAccount = await GetAccountById(accountId);
            return new CredentialsRes()
            {
                AccessToken     =   TokenUtility.GetAccessToken(accountId, selectedAccount.Type),
                RefreshToken    =   TokenUtility.GetRefreshToken(accountId),
            };
        }

        public async Task<Relation> GetRelation(string myAccountId, string accountId)
        {
            return await _appDbContext.Relations.SingleOrDefaultAsync(t=>t.FromAccountId==myAccountId && t.ToAccountId==accountId);
        }

        public Dictionary<string, Account> GetListOfFollowings(string accountId)
        {
            List<Account> list = new List<Account>();
            _appDbContext.Relations.Where(
                t=>t.FromAccountId==accountId &&
                t.RelationType==RelationTypes.Following
            ).ToList().ForEach(item =>
            {
                var acc = _appDbContext.Accounts.Find(item.ToAccountId);
                list.Add(acc);
            });

            var result = list
            .ToDictionary(
                t => t.AccountId,
                t => t
            );

            return result;
        }

        public Dictionary<string, Account> GetListOfFollowers(string accountId)
        {
            List<Account> list = new List<Account>();
            _appDbContext.Relations.Where(
                t=>t.ToAccountId==accountId &&
                t.RelationType==RelationTypes.Following
            ).ToList().ForEach(item =>
            {
                var acc = _appDbContext.Accounts.Find(item.FromAccountId);
                list.Add(acc);
            });

            var result = list
            .ToDictionary(
                t => t.AccountId,
                t => t
            );

            return result;
        }

        public Dictionary<string, Account> GetListOfBlocks(string accountId)
        {
            List<Account> list = new List<Account>();
            _appDbContext.Relations.Where(
                t=>t.FromAccountId==accountId &&
                t.RelationType==RelationTypes.Block
            ).ToList().ForEach(item =>
            {
                var acc = _appDbContext.Accounts.Find(item.ToAccountId);
                list.Add(acc);
            });

            var result = list
            .ToDictionary(
                t => t.AccountId,
                t => t
            );

            return result;
        }

        public async Task<long> GetNumOfFollowings(string accountId)
        {
            long result = await _appDbContext.Relations.Where(
                t=>t.FromAccountId == accountId &&
                t.RelationType == RelationTypes.Following
            ).CountAsync();

            return result;
        }

        public async Task<long> GetNumOfFollowers(string accountId)
        {
            long result = await _appDbContext.Relations.Where(
                t=>t.ToAccountId == accountId &&
                t.RelationType == RelationTypes.Following
            ).CountAsync();

            return result;
        }

        public async Task<Tweet> GetTweetById(string tweetId)
        {
            var selectedTweet = await _appDbContext.Tweets.FindAsync(tweetId);
            return selectedTweet;
        }
    }
}