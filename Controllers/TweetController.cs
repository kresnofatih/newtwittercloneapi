using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewTwitterCloneApi.Attributes;
using NewTwitterCloneApi.Constants;
using NewTwitterCloneApi.Data;
using NewTwitterCloneApi.Models;
using NewTwitterCloneApi.Models.DTOs;
using NewTwitterCloneApi.Utilities;

namespace NewTwitterCloneApi.Controllers
{
    [Route("api/[controller]")]
    public class TweetController : ControllerBase
    {
        public AppDbContext _appDbContext;

        public SupportController _supportController;

        public TweetController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _supportController = new SupportController(appDbContext);
        }

        [Auth]
        [HttpPost("post-new-tweet")]
        public async Task<ActionResult<Tweet>> PostNewTweet([FromBody]NewTweetReq newTweetReq)
        {
            var AccessToken = Request.Headers[HeaderTypes.AccessToken];
            var myAccountId = TokenUtility.ExtractAccountIdFromToken(AccessToken);

            var newTweet = GetTweetFromNewTweetReq(newTweetReq, myAccountId);
            await _appDbContext.Tweets.AddAsync(newTweet);
            await _appDbContext.SaveChangesAsync();

            return Ok(newTweet);
        }

        [Auth]
        [HttpPost("comment-tweet/{tweetId}")]
        public async Task<ActionResult<Tweet>> CommentTweet([FromBody]NewTweetReq newTweetReq, string tweetId)
        {
            var AccessToken = Request.Headers[HeaderTypes.AccessToken];
            var myAccountId = TokenUtility.ExtractAccountIdFromToken(AccessToken);

            var newTweet = GetCommentFromNewTweetReq(newTweetReq, myAccountId, tweetId);
            await _appDbContext.Tweets.AddAsync(newTweet);
            await _appDbContext.SaveChangesAsync();

            return Ok(newTweet);
        }

        [Auth]
        [HttpDelete("delete-tweet/{tweetId}")]
        public async Task<ActionResult<Tweet>> DeleteTweet(string tweetId)
        {
            var selectedTweet = await _supportController.GetTweetById(tweetId);
            _appDbContext.Remove(selectedTweet);
            await _appDbContext.SaveChangesAsync();

            return Ok();
        }

        private Tweet GetTweetFromNewTweetReq(NewTweetReq newTweetReq, string accountId)
        {
            return new Tweet()
            {
                TweetId = GuidUtility.GenerateGuid(),
                AccountId = accountId,
                Message = newTweetReq.Message,
                ImageUrl = newTweetReq.ImageUrl,
                CreatedAt = DateTime.UtcNow,
                CommentOfTweetId = null,
                RetweeterAccountId = null
            };
        }

        private Tweet GetCommentFromNewTweetReq(NewTweetReq newTweetReq, string accountId, string tweetId)
        {
            return new Tweet()
            {
                TweetId = GuidUtility.GenerateGuid(),
                AccountId = accountId,
                Message = newTweetReq.Message,
                ImageUrl = newTweetReq.ImageUrl,
                CreatedAt = DateTime.UtcNow,
                CommentOfTweetId = tweetId,
                RetweeterAccountId = null
            };
        }
    }
}