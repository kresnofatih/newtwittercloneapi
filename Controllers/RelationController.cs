using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewTwitterCloneApi.Attributes;
using NewTwitterCloneApi.Constants;
using NewTwitterCloneApi.Data;
using NewTwitterCloneApi.Models;
using NewTwitterCloneApi.Utilities;

namespace NewTwitterCloneApi.Controllers
{
    [Route("api/[controller]")]
    public class RelationController : ControllerBase
    {
        public AppDbContext _appDbContext;

        public SupportController _supportController;

        public RelationController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _supportController = new SupportController(appDbContext);
        }

        [Auth]
        [HttpPost("follow-account/{accountId}")]
        public async Task<ActionResult<Relation>> FollowAccount(string accountId)
        {
            var AccessToken = Request.Headers[HeaderTypes.AccessToken];
            var myAccountId = TokenUtility.ExtractAccountIdFromToken(AccessToken);

            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            var selectedRelation = await _supportController.GetRelation(myAccountId, accountId);
            var relationExists = _supportController.GetRelationExists(selectedRelation);
            if(!relationExists)
            {
                Relation newRelation = GetFollowingRelation(myAccountId, accountId);

                await _appDbContext.Relations.AddAsync(newRelation);
                await _appDbContext.SaveChangesAsync();

                return Ok(newRelation);
            }

            selectedRelation.RelationType = RelationTypes.Following;
            await _appDbContext.SaveChangesAsync();

            return Ok(selectedRelation);
        }

        [Auth]
        [HttpDelete("unfollow-account/{accountId}")]
        public async Task<IActionResult> UnfollowAccount(string accountId)
        {
            var AccessToken = Request.Headers[HeaderTypes.AccessToken];
            var myAccountId = TokenUtility.ExtractAccountIdFromToken(AccessToken);

            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            var selectedRelation = await _supportController.GetRelation(myAccountId, accountId);
            var relationExists = _supportController.GetRelationExists(selectedRelation);
            if(!relationExists)
            {
                return BadRequest("account does not exist");
            }
            _appDbContext.Relations.Remove(selectedRelation);
            await _appDbContext.SaveChangesAsync();
            return Ok();
        }

        [Auth]
        [HttpPost("block-account/{accountId}")]
        public async Task<IActionResult> BlockAccount(string accountId)
        {
            var AccessToken = Request.Headers[HeaderTypes.AccessToken];
            var myAccountId = TokenUtility.ExtractAccountIdFromToken(AccessToken);

            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            var selectedRelation = await _supportController.GetRelation(myAccountId, accountId);
            var relationExists = _supportController.GetRelationExists(selectedRelation);
            if(!relationExists)
            {
                Relation newRelation = GetBlockRelation(myAccountId, accountId);

                await _appDbContext.Relations.AddAsync(newRelation);
                await _appDbContext.SaveChangesAsync();
                return Ok(newRelation);
            }
            else
            {
                selectedRelation.RelationType = RelationTypes.Block;
                await _appDbContext.SaveChangesAsync();
                return Ok(selectedRelation);
            }
        }

        [Auth]
        [HttpPost("unblock-account/{accountId}")]
        public async Task<IActionResult> UnblockAccount(string accountId)
        {
            var AccessToken = Request.Headers[HeaderTypes.AccessToken];
            var myAccountId = TokenUtility.ExtractAccountIdFromToken(AccessToken);

            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            var selectedRelation = await _supportController.GetRelation(myAccountId, accountId);
            var relationExists = _supportController.GetRelationExists(selectedRelation);
            if(!relationExists)
            {
                return BadRequest("account does not exist");
            }
            else
            {
                _appDbContext.Remove(selectedRelation);
                await _appDbContext.SaveChangesAsync();
                return Ok();
            }
        }

        [Auth]
        [HttpGet("get-account-followings/{accountId}")]
        public async Task<ActionResult<Dictionary<string, Account>>> GetAccountFollowings(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            var listOfFollowings = _supportController.GetListOfFollowings(accountId);
            
            return Ok(listOfFollowings);
        }

        [Auth]
        [HttpGet("get-account-followers/{accountId}")]
        public async Task<ActionResult<Dictionary<string, Account>>> GetMyFollowers(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            var listOfFollowers = _supportController.GetListOfFollowers(accountId);
            
            return Ok(listOfFollowers);
        }

        [Auth]
        [HttpGet("get-following-number/{accountId}")]
        public async Task<ActionResult<long>> GetFollowingNumber(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            long numOfFollowing = await _supportController.GetNumOfFollowings(accountId);
            
            return Ok(numOfFollowing);
        }

        [Auth]
        [HttpGet("get-follower-number/{accountId}")]
        public async Task<ActionResult<long>> GetFollowerNumber(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            long numOfFollower = await _supportController.GetNumOfFollowers(accountId);
            
            return Ok(numOfFollower);
        }

        [Auth]
        [HttpGet("get-my-blocks/{accountId}")]
        public async Task<ActionResult<List<Account>>> GetMyBlocks(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account does not exist");
            }

            var listOfBlocks = _supportController.GetListOfBlocks(accountId);
            
            return Ok(listOfBlocks);
        }

        private Relation GetFollowingRelation(string myAccountId, string accountId)
        {
            return new Relation()
            {
                RelationId = GuidUtility.GenerateGuid(),
                FromAccountId = myAccountId,
                ToAccountId = accountId,
                RelationType = RelationTypes.Following
            };
        }

        private Relation GetBlockRelation(string myAccountId, string accountId)
        {
            return new Relation()
            {
                RelationId = GuidUtility.GenerateGuid(),
                FromAccountId = myAccountId,
                ToAccountId = accountId,
                RelationType = RelationTypes.Block
            };
        }
    }
}