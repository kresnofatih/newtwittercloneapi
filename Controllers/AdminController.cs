using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewTwitterCloneApi.Constants;
using NewTwitterCloneApi.Data;
using NewTwitterCloneApi.Models;

namespace NewTwitterCloneApi.Controllers
{
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        public AppDbContext _appDbContext;

        public SupportController _supportController;

        public AdminController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _supportController = new SupportController(appDbContext);
        }

        [HttpPut("blacklist-an-account/{accountId}")]
        public async Task<ActionResult<Account>> BlacklistAnAccount(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account doesn't exist");
            }

            ApplyBlacklistStatus(selectedAccount);
            _appDbContext.SaveChanges();

            return Ok(selectedAccount);
        }

        [HttpPut("make-admin-account/{accountId}")]
        public async Task<ActionResult<Account>> MakeAdminAccount(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account doesn't exist");
            }

            ApplyAdminStatus(selectedAccount);
            _appDbContext.SaveChanges();

            return Ok(selectedAccount);
        }

        [HttpPut("make-basic-account/{accountId}")]
        public async Task<ActionResult<Account>> MakeBasicAccount(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            var accountExists = _supportController.GetAccountExists(selectedAccount);
            if(!accountExists)
            {
                return BadRequest("account doesn't exist");
            }

            ApplyBasicStatus(selectedAccount);
            _appDbContext.SaveChanges();

            return Ok(selectedAccount);
        }

        [HttpDelete("delete-account/{accountId}")]
        public async Task<IActionResult> DeleteAccount(string accountId)
        {
            var selectedAccount = await _supportController.GetAccountById(accountId);
            
            _appDbContext.Accounts.Remove(selectedAccount);

            return Ok();
        }

        private void ApplyBlacklistStatus(Account account)
        {
            account.Type = AccountTypes.Blacklisted;
        }

        private void ApplyAdminStatus(Account account)
        {
            account.Type = AccountTypes.Admin;
        }

        private void ApplyBasicStatus(Account account)
        {
            account.Type = AccountTypes.Basic;
        }
    }
}