using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewTwitterCloneApi.Models
{
    public class Relation
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string RelationId {get; set;}

        [Required]
        public string FromAccountId {get; set;}

        [Required]
        public string ToAccountId {get; set;}

        [Required]
        public string RelationType {get; set;}
    }
}