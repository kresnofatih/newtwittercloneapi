namespace NewTwitterCloneApi.Models.DTOs
{
    public class ChangePasswordReq
    {
        public string OldPassword {get; set;}

        public string NewPassword {get; set;}
    }
}