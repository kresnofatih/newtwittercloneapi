using System.ComponentModel.DataAnnotations;

namespace NewTwitterCloneApi.Models.DTOs
{
    public class NewTweetReq
    {
        [Required]
        [MinLength(1)]
        [MaxLength(250)]
        public string Message {get; set;}

        public string ImageUrl {get; set;}
    }
}