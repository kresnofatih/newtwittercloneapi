namespace NewTwitterCloneApi.Models.DTOs
{
    public class CredentialsRes
    {
        public string AccessToken {get; set;}

        public string RefreshToken {get; set;}
    }
}