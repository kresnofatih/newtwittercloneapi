using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewTwitterCloneApi.Models
{
    public class Tweet
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string TweetId {get; set;}

        [Required]
        [MaxLength(20)]
        public string AccountId {get; set;}

        [Required]
        [MinLength(1)]
        [MaxLength(250)]
        public string Message {get; set;}

        public string ImageUrl {get; set;}

        [Required]
        public DateTime CreatedAt {get; set;}

        public string CommentOfTweetId {get; set;}

        public string RetweeterAccountId {get; set;}
    }
}