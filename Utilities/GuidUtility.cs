using System;

namespace NewTwitterCloneApi.Utilities
{
    public static class GuidUtility
    {
        public static string GenerateGuid()
        {
            return Guid.NewGuid().ToString()+Guid.NewGuid().ToString();
        }
    }
}