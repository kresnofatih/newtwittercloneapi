using System;

namespace NewTwitterCloneApi.Utilities
{
    public static class TimeUtility
    {
        public static DateTime GetDateTimeFromDate(int day, int month, int year)
        {
            var datetime = new DateTime(year, month, day, 0, 0, 0);
            return datetime;
        }

        public static long GetSecondsUtc(DateTime time)
        {
            return time.ToFileTimeUtc();
        }

        public static long GetNowSecondsUtc()
        {
            return DateTime.UtcNow.ToFileTimeUtc();
        }
    }
}