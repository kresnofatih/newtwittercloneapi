using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using NewTwitterCloneApi.Constants;

namespace NewTwitterCloneApi.Utilities
{
    public static class TokenUtility
    {
        public static string GetAccessToken(string accountId, string accountType)
        {
            if(accountId != null && accountType != null)
            {
                var secret = TokenSecret.SecretString;
                var tokenHandler = new JwtSecurityTokenHandler();
                long expireTime = DateTime.UtcNow.AddMinutes(50).ToFileTimeUtc();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject     =   GetAccessTokenClaimsIdentity(accountId, accountType, expireTime),
                    SigningCredentials  =   GetSigningCredentials(secret)
                };

                var stoken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(stoken);
                return token;
            }

            return null;
        }

        public static string GetRefreshToken(string accountId)
        {
            if(accountId != null)
            {
                var secret = TokenSecret.SecretString;
                var tokenHandler = new JwtSecurityTokenHandler();
                long expireTime = DateTime.UtcNow.AddHours(3).ToFileTimeUtc();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject     =   GetRefreshTokenClaimsIdentity(accountId, expireTime),
                    SigningCredentials  =   GetSigningCredentials(secret)
                };

                var stoken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(stoken);
                return token;
            }

            return null;
        }

        public static string RefreshAccessToken(string oldToken)
        {
            string accountId = ExtractAccountIdFromToken(oldToken);
            string accountType = ExtractAccountTypeFromToken(oldToken);
            string newToken = GetAccessToken(accountId, accountType);
            return newToken;
        }

        public static string RefreshRefreshToken(string oldToken)
        {
            string accountId = ExtractAccountIdFromToken(oldToken);
            string newToken = GetRefreshToken(accountId);
            return newToken;
        }

        private static ClaimsIdentity GetAccessTokenClaimsIdentity(string accountId, string accountType, long expireTime)
        {
            return new ClaimsIdentity(new[]
            {
                new Claim(  type:   TokenClaimTypes.AccountId,      value:  accountId),
                new Claim(  type:   TokenClaimTypes.AccountType,    value:  accountType),
                new Claim(  type:   TokenClaimTypes.ExpireTime,     value:  expireTime.ToString())
            });
        }

        private static ClaimsIdentity GetRefreshTokenClaimsIdentity(string accountId, long expireTime)
        {
            return new ClaimsIdentity(new[]
            {
                new Claim(  type:   TokenClaimTypes.AccountId,      value:  accountId),
                new Claim(  type:   TokenClaimTypes.ExpireTime,     value:  expireTime.ToString())
            });
        }

        private static SigningCredentials GetSigningCredentials(string secret)
        {
            var symmetricKey = Encoding.UTF8.GetBytes(secret);
            return new SigningCredentials(
                new SymmetricSecurityKey(symmetricKey),
                SecurityAlgorithms.HmacSha256Signature
            );
        }

        public static string ExtractAccountIdFromToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenS = tokenHandler.ReadToken(token) as JwtSecurityToken;
                var accountId = tokenS.Claims?.FirstOrDefault(claim => claim.Type == TokenClaimTypes.AccountId).Value;
                return accountId;
            }
            catch
            {
                return null;
            }
        }

        public static long ExtractExpireTimeFromToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenS = tokenHandler.ReadToken(token) as JwtSecurityToken;
                var expireTime = tokenS.Claims?.FirstOrDefault(claim => claim.Type == TokenClaimTypes.ExpireTime).Value;
                return Convert.ToInt64(expireTime);
            }
            catch
            {
                return 0;
            }
        }

        public static string ExtractAccountTypeFromToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenS = tokenHandler.ReadToken(token) as JwtSecurityToken;
                var accountType = tokenS.Claims?.FirstOrDefault(claim => claim.Type == TokenClaimTypes.AccountType).Value;
                return accountType;
            }
            catch
            {
                return null;
            }
        }

        public static string ExtractClaimValueFromToken(string token, string claimType)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenS = tokenHandler.ReadToken(token) as JwtSecurityToken;
                var claimValue = tokenS.Claims?.FirstOrDefault(claim => claim.Type == claimType).Value;
                return claimValue;
            }
            catch
            {
                return null;
            }
        }

        public static string RemoveBearerPrefix(string authHeader)
        {
            return authHeader.Replace("Bearer ", "");
        }
    }
}