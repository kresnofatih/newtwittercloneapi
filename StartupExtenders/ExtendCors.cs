using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NewTwitterCloneApi.Constants;

namespace NewTwitterCloneApi.StartupExtenders
{
    public static class ExtendCors
    {
        public static void AddTwitterCorsOptions(this IServiceCollection services)
        {
            services.AddCors(opt=>
            {
                opt.AddPolicy(
                    name: CorsPolicyTypes.MySpecificOrigins,
                    builder => {
                        builder.WithOrigins(
                            "http://localhost:3000"
                        )
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                    }
                );
            });
        }

        public static void UseTwitterCors(this IApplicationBuilder app)
        {
            app.UseCors(CorsPolicyTypes.MySpecificOrigins);
        }
    }
}