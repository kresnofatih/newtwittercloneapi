﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewTwitterCloneApi.Migrations
{
    public partial class retweeteraccountid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RetweetOfTweetId",
                table: "Tweets",
                newName: "RetweeterAccountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RetweeterAccountId",
                table: "Tweets",
                newName: "RetweetOfTweetId");
        }
    }
}
