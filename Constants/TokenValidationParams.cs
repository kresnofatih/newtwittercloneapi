using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace NewTwitterCloneApi.Constants
{
    public static class TokenValidationParams
    {
        public static readonly TokenValidationParameters Params = new TokenValidationParameters()
        {
            ValidateIssuer              = false,
            ValidateAudience            = false,
            ValidateIssuerSigningKey    = true,
            IssuerSigningKey            = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenSecret.SecretString))
        };
    }
}