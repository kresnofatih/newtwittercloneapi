namespace NewTwitterCloneApi.Constants
{
    public static class TokenClaimTypes
    {
        public const string AccountType = "AccountType";

        public const string AccountId = "AccountId";

        public const string EmailVerified = "EmailVerified";
        
        public const string ExpireTime = "ExpireTime";
    }
}