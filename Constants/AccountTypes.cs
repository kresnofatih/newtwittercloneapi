namespace NewTwitterCloneApi.Constants
{
    public static class AccountTypes
    {
        public const string Blacklisted = "Blacklisted";

        public const string Basic = "Basic";

        public const string Elevated = "Elevated";
        
        public const string Super = "Super";

        public const string Admin = "Admin";
    }
}